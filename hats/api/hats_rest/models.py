from django.db import models

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()


class Hat(models.Model):
    type = models.CharField(max_length=200, null=True)
    material = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name='hats',
        null=True,
        on_delete=models.CASCADE
    )
    def __str__(self):
        return self.color+self.type
