from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoe

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["id", "closet_name", "bin_number", "bin_size", "import_href"]


class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "shoe_name",
        "shoe_color",
        "shoe_photo_url",
        "bin",
    ]

    encoders = {"bin": BinVOEncoder()}

class ShoesDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "shoe_name",
        "shoe_color",
        "shoe_photo_url",
        "bin",
    ]

    encoders = {"bin": BinVOEncoder()}


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            shoes,
            encoder=ShoesListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            href = content["bin"]
            bin = BinVO.objects.get(id=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
                shoes,
                encoder=ShoesDetailEncoder,
                safe=False,
            )

@require_http_methods(["DELETE"])
def api_shoe(request, pk):
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})






#def shoes_view(request):
    #if request.method == 'POST':
        # This Handles the Shoe Creation
        #manufacturer = request.POST['manufacturer']
        #model_name = request.POST['model_name']
        #color = request.POST['color']
        #picture_url = request.POST['picture_url']
        #bin = request.POST['bin']
        #Shoe.objects.create(manufacturer=manufacturer, model_name=model_name, color=color, picture_url=picture_url, bin=bin)
        #return redirect('shoes')

    #elif request.method == 'GET':
        # This is gonna Handle shoe retrieval
        #shoes = Shoe.objects.all()
        #context = {'shoes': shoes}
        #return render(request, 'shoes.html', context)

    #elif request.method == 'PUT':
        # Handle shoe updating
        #shoe_id = request.POST['id']
        #manufacturer = request.POST['manufacturer']
        #model_name = request.POST['model_name']
        #color = request.POST['color']
        #picture_url = request.POST['picture_url']
        #bin = request.POST['bin']
        #shoe = Shoe.objects.get(id=shoe_id)
        #shoe.manufacturer = manufacturer
        #shoe.model_name = model_name
        #shoe.color = color
        #shoe.picture_url = picture_url
        #shoe.bin = bin
        #shoe.save()
        #return redirect('shoes')
