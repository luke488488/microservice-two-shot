from django.contrib import admin

from shoes_rest.models import Shoe, BinVO


@admin.register(Shoe)
class Shoe (admin.ModelAdmin):
    pass

@admin.register(BinVO)
class BinVO(admin.ModelAdmin):
    pass
