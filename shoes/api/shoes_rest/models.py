from django.db import models

# Create your models here.

class BinVO(models.Model):
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField(null=True, blank=True)
    bin_size = models.PositiveSmallIntegerField(null=True, blank=True)
    import_href = models.CharField(max_length=200, null=True, blank=True, unique=True)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    shoe_name = models.CharField(max_length=200, null=True)
    shoe_color = models.CharField(max_length=50, null=True)
    shoe_photo_url = models.URLField(blank=True, null=True)
    bin = models.ForeignKey(
        BinVO, related_name="shoe", null=True, blank=False, on_delete=models.CASCADE
    )
