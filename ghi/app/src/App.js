import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Hats from './Hats';
import HatForm from './HatForm';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';


function App() {
    const [hats, setHats] = useState([]);

  const fetchHats = async () => {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);

    if (response.ok){
      const data = await response.json();
      const hats = data.hats;
      setHats(hats);
    }
  };
const [shoes, setShoes] = useState([]);

  const getShoes = async () => {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const shoes = data;
      console.log(shoes);
      setShoes(shoes);
    }
  };

  useEffect (()=> {
    getShoes();
    fetchHats();
}, [])


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes/new" element={<ShoesForm getShoes= {getShoes}/>} />
          <Route path="/shoes" element={<ShoesList shoes={shoes} getShoes= {getShoes}/>} />
          <Route path="/hats" element={<Hats hats={hats} fetchHats={fetchHats} />} />
          <Route path="/hats/new" element={<HatForm fetchHats={fetchHats} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}



export default App;
