import React, { useState, useEffect } from 'react'
const HatForm = ({ fetchHats }) =>{
    const[type, setType] = useState('');
    const[material, setMaterial] = useState('');
    const[color, setColor] = useState('');
    const[picture, setPicture] = useState('');
    const[location, setLocation] = useState('');
    const[locations, setLocations] = useState([]);

    useEffect(() => {
        const locationVOUrl = "http://localhost:8100/api/locations/"
        fetch(locationVOUrl)
            .then(response => response.json())
            .then(data => setLocations(data.locations))
            .catch(e => console.error('error: ', e))
    }, [])

    const submit = (event) => {
        event.preventDefault();
        const newHat = {
            'type': type,
            'material': material,
            'color': color,
            'picture_url': picture,
            'location': location,
        }
        const hatsUrl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newHat),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(hatsUrl, fetchConfig)
            .then(response => {
                if (response.ok){
                    return fetchHats()
                }
            })
            .then(() => {
                setType('');
                setMaterial('');
                setColor('');
                setPicture('');
                setLocation('')
            })
            .catch(e => console.error('error: ', e))
    }

    const handleTypeChange = (event) => {
        const value = event.target.value;
        setType(value);
    }
    const handleMaterialChange = (event) => {
        const value = event.target.value;
        setMaterial(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    return(
        <form onSubmit={submit} id="create-hat-form">
            <div className="form-floating mb-3">
            <input value={type} onChange={handleTypeChange} id="type" type="text" name="type" maxLength={200} required />
            </div>
            <div className="form-floating mb-3">
            <input value={material} onChange={handleMaterialChange} id="material" type="text" name="material" maxLength={200} required />
            </div>
            <div className="form-floating mb-3">
            <input value={color} onChange={handleColorChange} id="color" type="text" name="color" maxLength={200} required />
            </div>
            <div className="form-floating mb-3">
            <input value={picture} onChange={handlePictureChange} id="picture" type="text" name="picture" required />
            </div>
            <div className="form-floating mb-3">
            <select value={location} onChange={handleLocationChange} id="location" name="location">
                <option value="">Choose Location</option>
                {locations.map(location => {
                    return(
                        <option key={location.id} value={location.href}>{location.closet_name}</option>
                    );
                })}
            </select>
            </div>
            <button className="btn btn-primary">Create</button>
        </form>
    )
}

export default HatForm;
