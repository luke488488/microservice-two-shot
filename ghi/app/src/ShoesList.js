import { NavLink } from 'react-router-dom';
function ShoesList(props) {
    const handleDelete = async (id) => {
        fetch('http://localhost:8080/api/shoes/'+id, {
            method: 'DELETE'
        }).then(() => {
            return props.getShoes()
        })
        .catch(console.log)
    }
    return(
        <div className="container">
        <table className="table table-striped">
            <thead>
                <tr>
                    <td>Manufacturer</td>
                    <td>Model Name</td>
                    <td>Color</td>
                    <td>Picture</td>
                    <td>Bin</td>
                    <td>Delete</td>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    console.log(shoe)
                    return(
                        <tr key={shoe.id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.shoe_name}</td>
                            <td>{shoe.shoe_color}</td>
                            <td><img src={shoe.shoe_photo_url} width="100px" height="100px" /></td>
                            <td>{shoe.bin.closet_name}</td>
                            <td align="center" ><button type='button' value={shoe.id} onClick ={() => handleDelete(shoe.id)}>Delete</button ></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        <NavLink className="nav-link" to="/shoes/new">Create new shoe</NavLink>
        </div>
    )
}
export default ShoesList;
