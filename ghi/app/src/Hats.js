import { NavLink } from 'react-router-dom';
function Hats(props) {
    const handleDelete = async (id) => {
        fetch('http://localhost:8090/api/hats/'+id, {
            method: 'DELETE'
        }).then(() => {
            return props.fetchHats()
        })
        .catch(console.log)
    }
    return(
        <div className="container">
        <table className="table table-striped">
            <thead>
                <tr>
                    <td>Type</td>
                    <td>Material</td>
                    <td>Color</td>
                    <td>Picture</td>
                    <td>Location</td>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    console.log(hat)
                    return(
                        <tr key={hat.id}>
                            <td>{hat.type}</td>
                            <td>{hat.material}</td>
                            <td>{hat.color}</td>
                            <td><img src={hat.picture_url} width="100px" height="100px" /></td>
                            <td>{hat.location.closet_name}</td>
                            <td align="center" ><button type='button' value={hat.id} onClick ={() => handleDelete(hat.id)}>Delete</button ></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        <NavLink className="nav-link" to="/hats/new">Create new hat</NavLink>
        </div>
    )
}

export default Hats;
