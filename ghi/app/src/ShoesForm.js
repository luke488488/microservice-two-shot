import React, {useEffect, useState} from 'react';

function ShoesForm({getShoes}) {

//this handles the submit of the form

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.shoe_name = shoeName;
        data.shoe_color = shoeColor;
        data.shoe_photo_url = shoePhotoUrl
        data.bin = bin;

        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        fetch(shoeUrl, fetchConfig)
            .then(response => {
                if (response.ok){
                    return getShoes()
                }
            })
            .then(() => {
              setmanufacturer('');
              setshoeName('');
              setshoeColor('');
              setshoeColor('');
              setshoePhotoUrl('');
              setbin('');
            })
            .catch(e => console.error('error: ', e))
        // const response = await fetch(shoeUrl, fetchConfig);
        // if (response.ok) {
        //   return getShoes();
        //   const newShoe = await response.json();

        //   console.log(newShoe);

        //   setmanufacturer('');
        //   setshoeName('');
        //   setshoeColor('');
        //   setshoeColor('');
        //   setshoePhotoUrl('');
        //   setbin('');
        // }
    }

// sets the values of the states depending on the value in our respective inputs

  const [manufacturer, setmanufacturer] = useState('');
  const handlemanufacturerChange = (event) => {
    const value = event.target.value;
    setmanufacturer(value);
  }

  const [shoeName, setshoeName] = useState('');
  const handleshoeNameChange = (event) => {
    const value = event.target.value;
    setshoeName(value);
  }

  const [shoeColor, setshoeColor] = useState('');
  const handleshoeColorChange = (event) => {
    const value = event.target.value;
    setshoeColor(value);
  }

  const [shoePhotoUrl, setshoePhotoUrl] = useState('');
  const handleshoePhotoUrlChange = (event) => {
    const value = event.target.value;
    setshoePhotoUrl(value);
  }

  const [bin, setbin] = useState([]);
  const handlebinChange = (event) => {
    const value = event.target.value;
    setbin(value);
  }


//States Stuff
const [bins, setBins] = useState([]);
useEffect(() => {
  const binVOUrl = "http://localhost:8100/api/bins/"
  fetch(binVOUrl)
      .then(response => response.json())
      .then(data => {
        setBins(data.bins)
        console.log(data.bins)
      })
      .catch(e => console.error('error: ', e))
}, [])

  // const [bins, setBins] = useState([]);
  // const fetchData = async () => {

  //   const url = 'http://localhost:8100/api/bins/';

  //   const response = await fetch(url);

  //   if (response.ok) {
  //     const data = await response.json();
  //     console.log(data.bins);
  //     setBins(data.bins);
  //   }
  // }
  // useEffect(() => {
  //   fetchData();
  // }, []);

// jsx

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Add a New Shoe!</h1>
        <form onSubmit={handleSubmit} id="create-conference-form">
          <div className="form-floating mb-3">
            <input onChange={handlemanufacturerChange} value={manufacturer} required type="text" name="manufacturer" id="manufacturer" className="form-control" />
            <label htmlFor="fabric">Manufacturer</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleshoeNameChange} value={shoeName} placeholder="Shoe Name" required type="text" name="shoeName" id="shoeName" className="form-control" />
            <label htmlFor="style_name">Shoe Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleshoeColorChange} value={shoeColor} placeholder="Shoe Color" required type="text" name="shoeColor" id="shoeColor" className="form-control" />
            <label htmlFor="color">Shoe Color</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleshoePhotoUrlChange} value={shoePhotoUrl} placeholder="Photo URL" required type="text" name="shoePhotoUrl" id="shoePhotoUrl" className="form-control" />
            <label htmlFor="picture">Shoe Photo URL</label>
          </div>
          <div className="mb-3">
            <select onChange={handlebinChange} value={bin} required name="bin" id="bin" className="form-select">
                <option value="">Bin</option>
                {bins.map(bin => {
                    return (
                        <option key={bin.href} value={bin.id}>
                        {bin.closet_name}
                        </option>
                    );
                })}
            </select>
          </div>
          <button className="btn btn-primary">Create Shoe</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default ShoesForm;
