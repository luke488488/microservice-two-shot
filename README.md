# Wardrobify

Team:

* Person 1 - Luke - Hats
* Person 2 - Gio - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

I made a hat model with the requred base values then added a foreign key to access the LocationVO model that I created to match the location model in the wardrobe api. I then made the poller create LocationVO objects for me to use by polling the wardrobe api and copying the values from wardrobes Location objects to the corresponding values in my LocationsVO model.
